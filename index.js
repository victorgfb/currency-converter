// Video tutorial seguido: https://www.youtube.com/watch?v=4W55nFDyIrc&t=642s

const puppeteer = require('puppeteer');
const readLineSync = require('readline-sync')

// console.log("oiii")

async function robot(){
    const browser = await puppeteer.launch({headless : false});
    const page = await browser.newPage();
    const moedaBase = readLineSync.question('Informe uma moeda base: ') || 'dolar'
    const moedaFinal = readLineSync.question('Informe uma moeda desejada: ') || 'real'
    const url = `https://www.google.com/search?client=opera&q=${moedaBase}+para+${moedaFinal}&sourceid=opera&ie=UTF-8&oe=UTF-8`
    await page.goto(url);


    // await page.screenshot({path: 'example.png'});
    

    const resultado = await page.evaluate(() => {
        return +document.querySelector('.a61j6.vk_gy.vk_sh.Hg3mWc').value
    });

    console.log(`O valor de 1 ${moedaBase} em ${moedaFinal} é ${resultado} `)

    await browser.close();
} 

robot()